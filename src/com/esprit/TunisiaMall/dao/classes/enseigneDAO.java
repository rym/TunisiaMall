/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.TunisiaMall.dao.classes;

import com.esprit.TunisiaMall.dao.interfaces.IenseigneDAO;
import com.esprit.TunisiaMall.entities.Enseigne;
import com.esprit.TunisiaMall.technique.DataSource;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static java.util.Arrays.stream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;


/**
 *
 * @author asus pc
 */
public class enseigneDAO implements IenseigneDAO{
    private static stockDAO instance = null; 
    private final Connection connection;
     

    public enseigneDAO() {
         connection=DataSource.getInstance().getConnection();
    }
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void add(Enseigne enseigne) {
   
       try {
           String query="INSERT INTO ENSEIGNE(nom_enseigne,ref_enseigne,type_vetement)Values(?,?,?) ";
           PreparedStatement ps=connection.prepareStatement(query);
           
           ps.setString(1, enseigne.getNom_enseigne());
           ps.setString(2, enseigne.getRef_enseigne());
           ps.setString(3, enseigne.getType_vetement());
           
           
            
           
           ps.executeUpdate();
       } catch (SQLException ex) {
           Logger.getLogger(enseigneDAO.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     @Override
   public void update(Enseigne enseigne)throws SQLException{
      try {

        String requete = "update enseigne set nom_enseigne=?, ref_enseigne=? ,type_vetement=? where id=?";
        PreparedStatement ps = connection.prepareStatement(requete);
            
            ps.setString(1, enseigne.getNom_enseigne());
            ps.setString(2, enseigne.getRef_enseigne());
            ps.setString(3, enseigne.getType_vetement());
            
           
            ps.setInt(4, enseigne.getId());
            ps.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
        }catch (SQLException ex) {
            System.out.println("error while updating" + ex.getMessage());
        }
    }
     
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
     @Override
     public void delete(Enseigne enseigne) {

        String requete = "delete from enseigne where id=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setInt(1, enseigne.getId());
            ps.executeUpdate();
            System.out.println("Suppression effectuée avec succès");
        } catch (SQLException ex) {
           
            System.out.println("Erreur lors de la suppression. " + ex.getMessage());
        }
    }
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
     public List<Enseigne> displayAll() {
 List<Enseigne> liste = new ArrayList<>();

        try {
            String requete = "select * from enseigne";
            Statement statement = connection.createStatement();
            ResultSet resultat = statement.executeQuery(requete);
            while (resultat.next()) {
                Enseigne e = new Enseigne();

                e.setId(resultat.getInt(1));
                e.setNom_enseigne(resultat.getString(2));
                e.setRef_enseigne(resultat.getString(3));
                e.setType_vetement(resultat.getString(4));
                

                System.out.println("e = +" + e);
                liste.add(e);
            }
            System.out.println(liste);
            return (liste);

        } catch (SQLException ex) {

            System.out.println("error while loading records " + ex.getMessage());
            return null;
        }
  }    
       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 public static stockDAO getInstance() {
        if (instance == null) {
            instance = new stockDAO();
            return instance;
        } else {
            return instance;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Enseigne> select() {
        return null;
    }
    
    
    
    
    
    
  public boolean ajouterImage(BufferedImage photo) {
        try {
            Enseigne e=new Enseigne();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(photo, "jpg", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            String requette = "insert into enseigne(id,image) values (?,?)";
            PreparedStatement preparedStatement =connection.prepareStatement(requette);
           preparedStatement.setInt(1,e.getId() );
            preparedStatement.setBytes(2, imageInByte);
            preparedStatement.executeUpdate();

        } catch (IOException | SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
        return true;
    }

  
  @Override
   public int Result1(String type){
         int resultat=0;
         Enseigne e = new Enseigne();
       for(Enseigne element : displayAll()){
           if(element.getNom_enseigne().equals("zara")){
               resultat++;
           }
       }
       return resultat;
   }
   
   @Override
   public int Result2(String type){
         int resultat=0;
         Enseigne e = new Enseigne();
       for(Enseigne element : displayAll()){
           if(element.getNom_enseigne().equals("channel")){
               resultat++;
           }
       }
       return resultat;
   }
   
   @Override
   public int Result3(String type){
         int resultat=0;
         Enseigne e = new Enseigne();
       for(Enseigne element : displayAll()){
           if(element.getNom_enseigne().equals("festina")){
               resultat++;
           }
       }
       return resultat;
   }
   
   @Override
   public int Result4(String type){
         int resultat=0;
         Enseigne e = new Enseigne();
       for(Enseigne element : displayAll()){
           if(element.getNom_enseigne().equals("guess")){
               resultat++;
           }
       }
       return resultat;
   }
   
}
