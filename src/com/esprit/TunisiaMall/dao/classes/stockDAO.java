/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.TunisiaMall.dao.classes;

import com.esprit.TunisiaMall.dao.interfaces.IstockDAO;
import com.esprit.TunisiaMall.entities.Stock;
import com.esprit.TunisiaMall.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author asus pc
 */
public class stockDAO implements IstockDAO{
    private static stockDAO instance = null;
    private final Connection connection; 

    public stockDAO() {
       connection=DataSource.getInstance().getConnection();
    }
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void add(Stock stock) {
      try {
           String query="INSERT INTO STOCK(nom_produit,type_vetement,qté,prix_v_d,prix_v_g,prix_achat)Values(?,?,?,?,?,?) ";
           PreparedStatement ps=connection.prepareStatement(query);
           ps.setString(1,stock.getNom_produit());
           ps.setString(2, stock.getType_vetement());
           ps.setInt(3, stock.getQte());
           ps.setFloat(4, stock.getPrix_v_d());
           ps.setFloat(5, stock.getPrix_v_g());
           ps.setFloat(6, stock.getPrix_achat());


            ps.executeUpdate();
       } catch (SQLException ex) {
           Logger.getLogger(stockDAO.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
     
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void update(Stock stock)throws SQLException {
      try {
            String requete = "update stock set nom_produit=?, type_vetement=?, qté=?, prix_v_d=?, prix_v_g=?, prix_achat=? where id=?";
            PreparedStatement ps = connection.prepareStatement(requete);
            
             ps.setString(1, stock.getNom_produit());
             ps.setString(2, stock.getType_vetement());
             ps.setInt(3, stock.getQte());
             ps.setFloat(4, stock.getPrix_v_d());
             ps.setFloat(5, stock.getPrix_v_g());
             ps.setFloat(6, stock.getPrix_achat());
            
             ps.setInt(7, stock.getId());
System.out.println(ps);
             ps.executeUpdate();
            System.out.println("Successfull update");
        } catch (SQLException ex) {
            System.out.println("error while updating" + ex.getMessage());
        }
    }
     //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
     public void delete(Stock stock) {

        String requete = "delete from stock where id=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setInt(1, stock.getId());
            ps.executeUpdate();
            System.out.println("Suppression effectuée avec succès");
        } catch (SQLException ex) {
           
            System.out.println("Erreur lors de la suppression. " + ex.getMessage());
        }
    }

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Stock> select() {
    return null;    
    }
    
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public List<Stock> Afficher() {

        List<Stock> liste = new ArrayList<>();

        try {
            String requete = "select * from stock";
            Statement statement = connection.createStatement();
            ResultSet resultat = statement.executeQuery(requete);
            while (resultat.next()) {
                Stock ss = new Stock();

                ss.setId(resultat.getInt(1));
                ss.setNom_produit(resultat.getString(2));
                ss.setType_vetement(resultat.getString(3));
                ss.setQte(resultat.getInt(4));               
                ss.setPrix_v_g(resultat.getFloat(5));
                ss.setPrix_v_d(resultat.getFloat(6));
                ss.setPrix_achat(resultat.getFloat(7));
                

                System.out.println("s = +" + ss);
                liste.add(ss);
            }
            System.out.println(liste);
            return (liste);

        } catch (SQLException ex) {

            System.out.println("error while loading records " + ex.getMessage());
            return null;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       public List<Object> findByReference(String nom_produit) {
        List<Object> stListe = new ArrayList<>();
        String requete = "select * from stock where nom_produit like '" + nom_produit + "%'";
        try {
            Statement statement = connection.prepareStatement(requete);
            ResultSet resultat = statement.executeQuery(requete);
            while (resultat.next()) {
                Stock ss = new Stock();
               ss.setId(resultat.getInt(1));
                ss.setNom_produit(resultat.getString(2));
                ss.setType_vetement(resultat.getString(3));
                ss.setQte(resultat.getInt(4));               
                ss.setPrix_v_g(resultat.getFloat(5));
                ss.setPrix_v_d(resultat.getFloat(6));
                ss.setPrix_achat(resultat.getFloat(7));
                stListe.add(ss);
            }
            return stListe;
        } catch (SQLException ex) {
            //Logger.getLogger(PersonneDao.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            return null;
        }
    }
       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 public static stockDAO getInstance() {
        if (instance == null) {
            instance = new stockDAO();
            return instance;
        } else {
            return instance;
        }
    }
}
