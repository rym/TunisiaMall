/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.TunisiaMall.dao.interfaces;

import com.esprit.TunisiaMall.entities.Enseigne;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author asus pc
 */
public interface IenseigneDAO {
     void add(Enseigne enseigne);
     void delete(Enseigne enseigne);
     void update(Enseigne enseigne)throws SQLException;
    List<Enseigne> select();
    public List<Enseigne> displayAll();
    int Result1 (String type);
    int Result2 (String type);
    int Result3 (String type);
    int Result4 (String type);
}
