/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.TunisiaMall.dao.interfaces;

import com.esprit.TunisiaMall.entities.Stock;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author asus pc
 */
public interface IstockDAO {
     void add(Stock stock);
     void update(Stock stock)throws SQLException;
     void delete(Stock stock);
     
    List<Stock> select();
}
