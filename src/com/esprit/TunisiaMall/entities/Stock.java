/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.TunisiaMall.entities;

/**
 *
 * @author asus pc
 */
public class Stock {
    private int id;
    private String nom_produit;
    private String type_vetement;
    private int qte;
    private Float prix_v_d;
    private Float prix_v_g;
    private Float prix_achat;

    
    

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    
   
    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }
    public String getNom_produit() {
        return nom_produit;
    }
      
      
    
    public void setType_vetement(String type_vetement) {
        this.type_vetement = type_vetement;
    }
    public String getType_vetement() {
        return type_vetement;
    }
    
   
    
    public int getQte() {
        return qte;
    }
    public void setQte(int qté) {
        this.qte = qté;
    }

    
    
    public Float getPrix_v_d() {
        return prix_v_d;
    }
     public void setPrix_v_d(Float prix_v_d) {
        this.prix_v_d = prix_v_d;
    }
   
     
     
    public Float getPrix_v_g() {
        return prix_v_g;
    }
    public void setPrix_v_g(Float prix_v_g) {
        this.prix_v_g = prix_v_g;
    }

    
    
    public Float getPrix_achat() {
        return prix_achat;
    }
    public void setPrix_achat(Float prix_achat) {
        this.prix_achat = prix_achat;
    }

    
}
