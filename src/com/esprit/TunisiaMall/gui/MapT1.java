package com.esprit.TunisiaMall.gui;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 * Afficher une carte GoogleMap dans un JEditorPane
 * @author fobec 2010
 */
public class MapT1 extends JEditorPane {

    private int zoomFactor = 18;
    private String ApiKey = "407013767805-te581k9mgjru4f77m4b6d3s1254kdj1j.apps.googleusercontent.com";
    private String roadmap = "roadmap";
    public final String viewTerrain = "terrain";
    public final String viewSatellite = "satellite";
    public final String viewHybrid = "hybrid";
    public final String viewRoadmap = "roadmap";
    static MapT1 map;
    /**
     * Constructeur: initialisation du EditorKit
     */
    public MapT1 () {
        HTMLEditorKit kit = new HTMLEditorKit();
        HTMLDocument htmlDoc = (HTMLDocument) kit.createDefaultDocument();
        this.setEditable(false);
        this.setContentType("text/html");
        this.setEditorKit(kit);
        this.setDocument(htmlDoc);
    }

    /**
     * Fixer le zoom
     * @param zoom valeur de 0 Ã  21
     */
    public void setZoom(int zoom) {
        this.zoomFactor = zoom;
    }

    /**
     * Fixer la clÃ© de developpeur
     * @param key APIKey fourni par Google
     */
    public void setApiKey(String key) {
        this.ApiKey = key;
    }

    /**
     * Fixer le type de vue
     * @param roadMap
     */
    public void setRoadmap(String roadMap) {
        this.roadmap = roadMap;
    }

    /**
     * Afficher la carte d'aprÃ¨s des coordonnÃ©es GPS
     * @param latitude
     * @param longitude
     * @param width
     * @param height
     * @throws Exception erreur si la APIKey est non renseignÃ©e
     */
    public void showCoordinate(String latitude, String longitude, int zoom) throws Exception {
        this.setMap(latitude, longitude, zoom);
    }

    /**
     * Afficher la carte d'aprÃ¨s une ville et son pays
     * @param city
     * @param country
     * @param width
     * @param height
     * @throws Exception erreur si la APIKey est non renseignÃ©e
     */
    public void showLocation(String city, String adresse, int zoom) throws Exception {
        map.setApiKey("407013767805-te581k9mgjru4f77m4b6d3s1254kdj1j.apps.googleusercontent.com");
        this.setMap(city, adresse,zoom);
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.add(map);
        frame.setSize(600, 620);
        frame.setLocation(200, 200);
        frame.setVisible(true);
    }

    /**
     * Assembler l'url et GÃ©nÃ©rer le code HTML
     * @param x
     * @param y
     * @param width
     * @param height
     * @throws Exception
     */
    private void setMap(String ville, String adresse,int zoom) throws Exception {
        if (this.ApiKey.isEmpty()) {
            throw new Exception("Developper API Key not set !!!!");
        }

        String url = "http://maps.google.com/maps/api/staticmap?";
        url += "center=" + ville + "," + adresse;
        url += "&zoom=" + zoom;
        url += "&size=600x600";
        url += "&maptype=" + this.viewRoadmap;
        url += "&markers=color:red%7C" + ville + "," +adresse;
        url += "&sensor=true";
        //url += "&key=" + this.ApiKey;
        String html = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>";
        html += "<html><head></head><body>";
        html += "<img src='" + url + "'>";
        html += "</body></html>";
        this.setText(html);
    }

    /**
     * Exemple : JGoogleMapEditorPan dans un JFrame
     */
    public static void main(String[] args) {
        try {
            map = new MapT1 ();
            map.showLocation("tunis", "ariana", 15);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}